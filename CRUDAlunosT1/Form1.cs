﻿using CRUDAlunosT1.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDAlunosT1
{
    public partial class Form1 : Form
    {
        List<Aluno> ListaDeAluno = new List<Aluno>();




        public Form1()
        {
            InitializeComponent();

            ListaDeAluno.Add(new Aluno{IdAluno = 1,PrimeiroNome = "Afonso",Apelido = "Monteiro"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 2,PrimeiroNome = "André",Apelido = "Gomes"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 3,PrimeiroNome = "André",Apelido = "Nunes"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 4,PrimeiroNome = "Andrii",Apelido = "Atamanchuk"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 5,PrimeiroNome = "Bruno",Apelido = "Teixeira"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 6,PrimeiroNome = "Denilson",Apelido = "Santana"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 7,PrimeiroNome = "Diogo",Apelido = "Martins"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 8,PrimeiroNome = "Dorival",Apelido = "Gonga"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 9,PrimeiroNome = "Edgar",Apelido = "Cardoso"
            });
            ListaDeAluno.Add(new Aluno{IdAluno = 10,PrimeiroNome = "Fábio",Apelido = "Barradas"
            });

            LoadListas();
        }

        private void LoadListas()
        {
            ComboBoxTodosAlunos.DataSource = ListaDeAluno;

            //ComboBoxTodosAlunos.DisplayMember = "NomeCompleto";

            listBoxTodosAlunos.DataSource = ListaDeAluno;
            listBoxTodosAlunos.DisplayMember = "NomeCompleto";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show(this,"Escolha o criterio","Cabeçudo!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            else if (comboBoxProcurar.SelectedIndex == 0)
            {
                MessageBox.Show("Escolheu procurar por Id de Aluno!");
            }

            else
            {
                MessageBox.Show("Escolheu procurar por Apelido!");
            }
        }
    }
}
